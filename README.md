# Demo of the containerized Bluesky

**Base images of Ubuntu 20 and 22 seem to be more complicated to deploy becuase of issues with wheel.**

 Latest image for tiled and for the bluesky container was built is based on python:3.10-slim-bullseye.


To make tiled work it's necessary to have:

```
python3 -m pip install --upgrade databroker[all]
```

To deploy all the services use:
```
docker-compose up
```

## How to run the demo

In order to make it work you need to have the beamline tools somewhere on your PC.
This path should be specified in the .env file. That's the prerequisite for this compose to work.

## Tiled 

Tiled web interface can be accessed at: [tiled web](localhost:8000). 

Don't forget about the authentication if you specified api_key. That's not good for production. 

For the tiled to work properly the config file has to be included.

# Bluesky container 

When the container is started the bluesky_start_root command is issued, so the container starts in the profile_root ipython session, configured with the basic beamline

#### Building ####

    sudo docker build -t bluesky_container:latest .

or

    sudo docker build --no-cache -t bluesky_container:latest .

#### Run the container ####
    docker run -i -t bluesky_container

#### RUn the container with x11 forwarding ####

    xhost +local:docker
    docker run -it --rm -e DISPLAY=$DISPLAY -v /tmp/.X11-unix:/tmp/.X11-unix bluesky_container

or mounting the local volume beamlinetools (change the location of beamlinetools for the one on your computer)

    docker run -it --rm  -e DISPLAY=$DISPLAY \
                        -v /tmp/.X11-unix:/tmp/.X11-unix \
                        -v /home/lporzio/Projects/beamlinetools:/opt/bluesky/beamlinetools bluesky_container \
                        sh -c  "python3 -m pip install -e /opt/bluesky/beamlinetools && bash"

    docker run -it --rm  -e DISPLAY=$DISPLAY \
                    -v /tmp/.X11-unix:/tmp/.X11-unix \
                    -v /home/simone/Documents/bluesky/containers/beamlinetools:/opt/bluesky/beamlinetools bluesky_container \
                    sh -c  "python3 -m pip install -e /opt/bluesky/beamlinetools && bash"

#### Push to registry ####

    docker login registry.hzdr.de

    docker build -t registry.hzdr.de/hzb/bluesky/containers/beamline:latest .

    docker push registry.hzdr.de/hzb/bluesky/containers/beamline


#### Instructions for Dockerfile ####

    https://docs.docker.com/engine/reference/builder/


#### Run bluesky in the container ####

    ipython --ipython-dir=/opt/bluesky/ipython --profile=root

#### Make a scan in bluesky using simulated devices ####

    RE(dscan([noisy_det], motor, -10,10,10))

or using the magics:

    %dscan [noisy_det] motor -1 1 10


#### GIT - submodules error ####
This hack will reset the submodules to the last valid commit in the beamline_test project. Any change you made will be lost in the submodules.

    git submodule update --remote
    git add . && git commit -m 'fix submodules error' && git push

#### GIT - remember user and pw ####

    git config --global credential.helper 'cache --timeout=3600'b

